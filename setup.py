import os
from setuptools import setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='django-calendar',
    version='0.1',
    packages=['djcalendar'],
    include_package_data=True,
    license='Creative Commons Attribution-ShareAlike 4.0 International',
    description='A simple Django calendar based on your custom model.',
    long_description=README,
    url='https://bitbucket.org/niklak/django-calendar/',
    author='Nikolay Gumanov',
    author_email='morgenpurple@gmail.com',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)