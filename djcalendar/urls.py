from django.conf.urls import patterns, url
from djcalendar import views


urlpatterns = patterns('',
	url(r'^get_table/$', views.get_calendar_table, name='table'),
)
